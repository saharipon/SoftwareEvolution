package assn.input.math;



public class BinaryOperation {
	
	   public static int gcd(int p, int q) throws Exception {
	        int a = p;
	        int b = q;
	        if (a == 0 ||
	            b == 0) {
	            if (a == Integer.MIN_VALUE ||
	                b == Integer.MIN_VALUE) {
	                throw new Exception();
	            }
	            return abs(a + b);
	        }

	        long al = a;
	        long bl = b;
	        boolean useLong = false;
	        if (a < 0) {
	            if(Integer.MIN_VALUE == a) {
	                useLong = true;
	            } else {
	                a = -a;
	            }
	            al = -al;
	        }
	        if (b < 0) {
	            if (Integer.MIN_VALUE == b) {
	                useLong = true;
	            } else {
	                b = -b;
	            }
	            bl = -bl;
	        }
	        if (useLong) {
	            if(al == bl) {
	                throw new Exception();
	            }
	            long blbu = bl;
	            bl = al;
	            al = blbu % al;
	            if (al == 0) {
	                if (bl > Integer.MAX_VALUE) {
	                    throw new Exception();
	                }
	                return (int) bl;
	            }
	            blbu = bl;

	            // Now "al" and "bl" fit in an "int".
	            b = (int) al;
	            a = (int) (blbu % al);
	        }

	        return gcdPositive(a, b);
	    }

	    /**
	     * Computes the greatest common divisor of two <em>positive</em> numbers
	     * (this precondition is <em>not</em> checked and the result is undefined
	     * if not fulfilled) using the "binary gcd" method which avoids division
	     * and modulo operations.
	     * See Knuth 4.5.2 algorithm B.
	     * The algorithm is due to Josef Stein (1961).
	     * <br/>
	     * Special cases:
	     * <ul>
	     *  <li>The result of {@code gcd(x, x)}, {@code gcd(0, x)} and
	     *   {@code gcd(x, 0)} is the value of {@code x}.</li>
	     *  <li>The invocation {@code gcd(0, 0)} is the only one which returns
	     *   {@code 0}.</li>
	     * </ul>
	     *
	     * @param a Positive number.
	     * @param b Positive number.
	     * @return the greatest common divisor.
	     */
	    private static int gcdPositive(int a, int b) {
	        if (a == 0) {
	            return b;
	        }
	        else if (b == 0) {
	            return a;
	        }

	        // Make "a" and "b" odd, keeping track of common power of 2.
	        final int aTwos = Integer.numberOfTrailingZeros(a);
	        a >>= aTwos;
	        final int bTwos = Integer.numberOfTrailingZeros(b);
	        b >>= bTwos;
	        final int shift = Math.min(aTwos, bTwos);

	        // "a" and "b" are positive.
	        // If a > b then "gdc(a, b)" is equal to "gcd(a - b, b)".
	        // If a < b then "gcd(a, b)" is equal to "gcd(b - a, a)".
	        // Hence, in the successive iterations:
	        //  "a" becomes the absolute difference of the current values,
	        //  "b" becomes the minimum of the current values.
	        while (a != b) {
	            final int delta = a - b;
	            b = Math.min(a, b);
	            a = Math.abs(delta);

	            // Remove any power of 2 in "a" ("b" is guaranteed to be odd).
	            a >>= Integer.numberOfTrailingZeros(a);
	        }

	        // Recover the common power of 2.
	        return a << shift;
	    }

	    /**
	     * <p>
	     * Gets the greatest common divisor of the absolute value of two numbers,
	     * using the "binary gcd" method which avoids division and modulo
	     * operations. See Knuth 4.5.2 algorithm B. This algorithm is due to Josef
	     * Stein (1961).
	     * </p>
	     * Special cases:
	     * <ul>
	     * <li>The invocations
	     * {@code gcd(Long.MIN_VALUE, Long.MIN_VALUE)},
	     * {@code gcd(Long.MIN_VALUE, 0L)} and
	     * {@code gcd(0L, Long.MIN_VALUE)} throw an
	     * {@code ArithmeticException}, because the result would be 2^63, which
	     * is too large for a long value.</li>
	     * <li>The result of {@code gcd(x, x)}, {@code gcd(0L, x)} and
	     * {@code gcd(x, 0L)} is the absolute value of {@code x}, except
	     * for the special cases above.
	     * <li>The invocation {@code gcd(0L, 0L)} is the only one which returns
	     * {@code 0L}.</li>
	     * </ul>
	     *
	     * @param p Number.
	     * @param q Number.
	     * @return the greatest common divisor, never negative.
	     * @throws MathArithmeticException if the result cannot be represented as
	     * a non-negative {@code long} value.
	     * @since 2.1
	     */
	    public static long gcd(final long p, final long q) throws Exception {
	        long u = p;
	        long v = q;
	        if ((u == 0) || (v == 0)) {
	            if ((u == Long.MIN_VALUE) || (v == Long.MIN_VALUE)){
	                throw new Exception();
	            }
	            return Math.abs(u) + Math.abs(v);
	        }
	        // keep u and v negative, as negative integers range down to
	        // -2^63, while positive numbers can only be as large as 2^63-1
	        // (i.e. we can't necessarily negate a negative number without
	        // overflow)
	        /* assert u!=0 && v!=0; */
	        if (u > 0) {
	            u = -u;
	        } // make u negative
	        if (v > 0) {
	            v = -v;
	        } // make v negative
	        // B1. [Find power of 2]
	        int k = 0;
	        while ((u & 1) == 0 && (v & 1) == 0 && k < 63) { // while u and v are
	                                                            // both even...
	            u /= 2;
	            v /= 2;
	            k++; // cast out twos.
	        }
	        if (k == 63) {
	            throw new Exception();
	        }
	        // B2. Initialize: u and v have been divided by 2^k and at least
	        // one is odd.
	        long t = ((u & 1) == 1) ? v : -(u / 2)/* B3 */;
	        // t negative: u was odd, v may be even (t replaces v)
	        // t positive: u was even, v is odd (t replaces u)
	        do {
	            /* assert u<0 && v<0; */
	            // B4/B3: cast out twos from t.
	            while ((t & 1) == 0) { // while t is even..
	                t /= 2; // cast out twos
	            }
	            // B5 [reset max(u,v)]
	            if (t > 0) {
	                u = -t;
	            } else {
	                v = t;
	            }
	            // B6/B3. at this point both u and v should be odd.
	            t = (v - u) / 2;
	            // |u| larger: t positive (replace u)
	            // |v| larger: t negative (replace v)
	        } while (t != 0);
	        return -u * (1L << k); // gcd is u*2^k
	    }
	    public static int abs(final int x) {
	        final int i = x >>> 31;
	        return (x ^ (~i + 1)) + i;
	    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
