#!/bin/bash

# Compiles sources and runs the Main class.

rm -rf bin
mkdir -p bin
javac -d bin ./src/assn/input/math/BinaryOperation.java ./src/main/Main.java
java -classpath bin main.Main
